#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

SITENAME = u'D\xe9tecteur de casseroles'
SITEURL = ''

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'
THEME = "zurb-F5-basic"

SITEURL="http://casseroles.hatrix.fr"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS =  (('Pelican', 'http://getpelican.com/'),
          ('Python.org', 'http://python.org/'),
          ('Jinja2', 'http://jinja.pocoo.org/'),
         )

# Social widget
SOCIAL = (('bitbucket', 'https://bitbucket.org/hatrix/d-tecteur-de-casserole/'),
         )

DEFAULT_PAGINATION = 10

CUSTOM_MENUITEMS = (
                    ('Membres', 'pages/members.html'),
                    ('Téléchargements', 'pages/downloads.html'),
                    ('Contact', 'pages/contact.html'),
                   )

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
