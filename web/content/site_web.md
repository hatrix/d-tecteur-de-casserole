Title: Un site web !
Date: 2016-03-13 12:12
Category: misc
Athors: Maël 'hatrix' Le Garrec

Voici donc notre site web ! Il utilise Pelican, ce qui nous permet d'écrire
seulement nos articles en markdown, pas de prise de tête.

À bientôt pour la suite !
