Title: Recherche de fréquence
Date: 2016-03-14 10:12
Category: fréquences
Athors: Maël 'hatrix' Le Garrec

La recherche de fréquence est elle aussi aisée. Après avoir trouvé l'amplitude
maximale de notre fenêtre, il est possible de trouver la fréquence qui lui
correspond. Les fréquences se suivent en effet et il est possible de déduire
celle qui nous intéresse.

les fréquences sont divisées selon le *sample rate* et la taille de la fenêtre.
On peut ainsi diviser les fréquences via cette formule :

frequences = n * Fs / N

où *frequences* représente l'intervalle recherché, *n* la position de la valeur
dans la liste, *Fs* le *samplerate* et *N* la taille de la fenêtre.

Par exemple, pour un samplerate de 44100Hz, une fenêtre de 4096 et un *n* 
trouvé à 93, on trouve 1001Hz pour un son de 1kHz. Cette approximation est 
inévitable de par la nature de la FFT.
