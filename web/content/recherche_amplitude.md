Title: Recherche d'amplitude
Date: 2016-03-14 10:10
Category: fréquences
Athors: Maël 'hatrix' Le Garrec

Une transformation de Fourier rapide sur nos données nous permet d'obtenir des
nombres complexes. Cette transformation doit s'effectuer sur des fenêtres, un
intervalle arbitraire de données pour pouvoir fonctionner correctement.\
En effet, si la fenêtre est trop petite, la fréquence ne peut être trouvée de
manière sûre.

Une FFT est donc effectuée sur notre intervalle. La liste de complexes en
résultat nous permet de trouver les amplitudes et donc la note fondamentale
d'une partie d'un son.\
Cette amplitude se calcule via la norme du complexe :

amplitude = sqrt(re^2 + im^2)

On obtient ainsi une liste d'amplitude pour chaque complexe. L'amplitude 
maximale définit notre fondamentale. Les harmoniques ne nous sont pas utiles.
