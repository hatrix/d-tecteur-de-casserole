Title: Membres
Date: 2014-09-12
Author: Maël Le Garrec

### Mael "hatrix" Le Garrec

L'informatique m'attire depuis toujours. Mais c'est au cours de mes années à 
EPITA que j'ai commencé à découvrir ce qui m'intéresse vraiment.\
Mon centre d'intérêt principal aujourd'hui est en effet la programmation bas
niveau et toute la technicité qui en découle. Je compte en effet parmis mes
projets différents émulateurs applications système.

Le langage que je maitrise le mieux à ce jour est le Python. Il est en effet 
très utile pour réaliser nombre de scripts, que ce soit pour réaliser des 
programmes en eux-mêmes ou pour travailler facilement sur du bas niveau.\
Les CTFs du LSE m'intéressent beaucoup et c'est notamment grâce à ce langage
que j'ai pu un jour m'hisser dans le top10.

Mon projet de première année, non pas un jeu vidéo comme la plupart de mes
camarades, était un vrai challenge. Il était ici question d'un ordinateur, crée
à partir de la porte logique NAND. Je me suis occupé de la partie bas niveau :
création de l'ALU, du processeur puis de l'ordinateur complet au niveau 
logique. Je me suis également occupé de l'assembleur : la création des opcodes
nécessaires et utiles ainsi que le programme permettant à l'ordinateur émulé 
de l'exécuter.

L'an dernier, un OCR réalisé en C m'a permis de vraiment appronfondir ce 
langage, mal connu auparavant. Je ne l'utilisais en effet que très peu de par
sa difficulté et son manque de résultats rapides et faciles. Ce projet m'a 
permis de réaliser l'étendue de son potentiel et je l'utilise depuis 
régulièrement.

Le projet de cette année, faisant intervenir des analyses de son me permet de
me familiariser avec cet univers, encore méconnu de ma part. Il ne représente
en soi par une réelle difficulté d'un point de vue technique, mais surtout d'un
point de vue recherche, les notions requises n'étant pas triviales et non 
abordées en cours.

### Hélène Ksiezak

Contrairement a la grande majorité d'élèves étant a epita car ingénieur en
informatique est ce qu'ils veulent être, je dois admettre que dans mon cas, je
suis tombée ici par pur hasard. En effet, j'ai intégré cette école en sachant
faire un copier/coller et rien d'autre. Je n'ai fais ni la spé ISN en terminale,
ni de petit projet perso sur la calculatrice, de petits sites web: je ne me suis
pas du tout aventurée dans le monde de l'informatique. Cela peut paraitre
completement fou de décider sur un coup de tête d'intégrer une école
d'informatique mais je ne le regrette pas du tout. 
Certes, par rapport aux autres élèves j'ai un niveau très bas mais il ne faut
pas s'arrêter là!

Ainsi, en première année j'ai pu découvrir ce qu'était un peu plus
l'informatique en travaillant en groupe sur un projet de jeu vidéo. Et je dois
avouer que si j'ai appris une quantité de choses en participant au projet,
travailler en groupe m'a un peu dégouté.

Au commencement de notre deuxième année dans cette école, on nous a attribué la
tâche de réaliser une reconnaissance faciale. Si le sujet avait l'air aussi
effrayant qu'intéressant, c'est le travail en groupe qui m'a vraiment marqué: il
était inéxistant. Doté d'un piètre niveau comme le mien, ce fut donc très
difficile et fatigant d'essayer de réaliser un projet de ce niveau toute seule.
Et le résultat ne fut pas très étonnant: on avait pas du tout ce qui nous était
demandé. Cependant, je tiens à dire que l'expérience était unique car beaucoup
d'élèves d'autres groupes sont venus a mon secour et j'ai pu ainsi apprendre
énomément de choses.

Suite a ce qu'il s'était passé lors de cette soutenance, je ne vais pas dire que
j'ai été traumatisé par le terme “projet” mais on s'en rapproche assez. Ainsi,
lorsqu'il a fallut créer des nouveaux groupes pour le S4, j'ai tout de suite
chercher a être avec quelqu'un que je connaissais et qui était bon en code:
Brendan. Malheureusement, j'ai été vraiment déçue du sujet choisi mais les
autres membres du groupe étant tellement enchanté qu'il a fallut que je m'y
colle.

### Brendan "Exy" Harley

Passionné par l'informatique depuis le plus jeune âge, j'ai aujourd'hui une
expérience grandissante avec la programation. Des premiers jeux vidéos à la
découverte de comment fonctionnent les programmes, pour enfin faire mes premiers
scripts en shell, j'ai toujours eu envie de découvrir plus. Depuis mon entrée a Epita, j'ai eu
l'occasion de manipuler de nombreux aspects de la programation, tant du coté
orienté objet que fonctionnel.

L'année dernière j'ai ainsi eu l'occasion de développer un jeu video à l'aide
du moteur de jeu video Unity. J'ai pu comprendre grâce a ce projet la mise en
place d'un réseau, d'une interface graphique et de gestion d'un projett d'une
taille assez conséquente. Lors de mon troisième semestre, notre projet fut une
reconnaissance faciale. Celle-ci se découpa en deux temps, tout d'abord nous
devions détecter s'il y avait un visage au sein de l'image, pour enfin pouvoir
l'identifier. La detection s'effectua à l'aide de l'algorithme de Viola et
Jones, l'identification à l'aide de la méthode dite des "eigenfaces".

Aujourd'hui j'ai la chance de pouvoir mettre ces compétences pour un projet qui
me semble intéressant.Ce projet est pour moi une occasion de découvrir un nouvel élément
de travail : le son.
