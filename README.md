# Introduction

Bienvenue sur le détecteur de Casseroles !
Ce programme a pour but de répliquer le fonctionnement de *Shazam*.

# Architecture

Vous trouverez dans le dossier *doc/* les documents suivants :

- Cahier des charges
- Rapport de soutenance 1
- Rapport de soutenance 2
- Rapport de projet

Dans le dossier *src/* se trouvent le code source de notre projet. Un Makefile
est également présent.
Pour utiliser notre programme dans son ensemble, trois fichiers sont à compiler :

* tests/micro_test.c
* tests/recognise.c
* tests/create_db.c


# Compilation

Ces deux fichiers peuvent être compilés via *make micro_test*, 
*make recognise* et *make create_db*.
Comme leurs noms l'indiquent, l'un permet de reconnaître une musique tandis que
l'autre enregistre des empreintes dans la base de données.


# Utilisation

## Enregistrement micro

Pour enregistrer le micro, il suffit de lancer cette commande :

```
./tests/micro_test output.wav duration
```

## Base de données

Pour enregistrer des musiques dans la base de données, il faut donner en 
argument au programme un dossier. Ce dossier se doit de respecter cette 
architecture : NomArtiste/Album/musiques.{flac,mp3,ogg,…}.
Les musiques sont automatiquement converties en wav dans /tmp/ pour pouvoir 
être traitées.

On utilise ainsi :

```
./tests/create_db ~/Musique/Les\ Fatals\ Picards/
```

## Reconnaissance

Pour reconnaître une musique, c'est vraiment très simple. Sont alors données 
trois musiques ayant la plus grande ressemblance avec l'extrait.

```
./tests/recognise file.wav
```

