﻿# Introduction

Il arrive souvent de se demander, en écoutant la radio ou au bar, quelle est la
musique que l'on entend. Souvent, il suffit de demander aux personnes aux
alentours afin d'avoir une réponse plus ou moins correcte.

Mais voilà, ce système n'est pas fiable. Un ordinateur ferait-il mieux ?
C'est le but de notre projet : « le détecteur de casseroles ». Celui-ci a en 
effet pour but de reconnaître des musiques. Il est donc similaire
à l'application existante « Shazam ».

Le déroulement de ce projet s'étalera sur plusieurs mois et sera finalisé pour
la semaine du 23 mai 2016.

\newpage

# Le groupe

Le groupe s'est formé en 3 temps. Le premier débuta quand Adrien et Brendan
apprirent qu'il avaient la chance de passer leur quatrième semestre dans leur
école au lieu de partir à l'étranger comme tous leurs camarades. Ils décidérent
alors qu'ils formeraient un groupe pour leur 'S4'. Après quelques réflexions,
et de nombreuses confrontations sur ce quoi porterait le projet, ils
proposèrent à Helene de les rejoindre, sans pour autant être plus avancé dans
leur recherche de sujet.Puis vint Maël, qui leur proposa un projet de
reconnaissance sonore. Ce projet leur plu car ils venaient de finir un projet
de reconnaissance faciale, n'étant pas un franc succès, ils voulurent faire
mieux avec un projet un peu plus facile. Ensemble, ils décidèrent de prendre le
nom "Détecteurs de Casseroles" comme clin d'oeil aux nombreuses musiques
populaire étant auditivement stercoraires.

\newpage

# Problématique

Le but final de notre projet est de pouvoir reconnaître des sons. Ainsi, il
sera possible d'établir une correspondance entre le son enregistré et la base
de donnée existante dans son état actuel.

Plusieurs problèmes se posent : comment caractériser un son ? Comment le 
reconnaître à partir d'un échantillon ? Comment reconnaître un son à partir 
d'un enregistrement avec du bruit ?

# Empreinte d'un son

## Fréquence et amplitude

Un son est, physiquement, une onde. Ce qui nous intéresse ici, est plus 
particulièrement la musique. Chaque musique est ainsi composée de fréquences
différentes. L'amplitude du signal varie également.

Il est possible de caractériser un son grâce à ces fréquences couplées à 
l'amplitude. Le but ici est de ne retenir que les amplitudes les plus fortes
afin d'éviter de prendre en compte un quelconque bruit de fond lors d'un 
enregistrement.

## Empreinte

Une fois les couples temps/fréquence récupérés sur les pics à amplitude forte,
il est théoriquement possible de reconnaître un son. un autre problème survient
toutefois : il est fort probable qu'un autre son produise un spectrogramme 
similaire.

Il faut donc créer une meilleure empreinte du son. Celle-ci peut-être améliorée
en liant chaque pic d'amplitude à un autre, par exemple avec le temps les 
séparant. Cette technique permet ainsi d'avoir une empreinte plus forte pour un
son donné, réduisant le nombre de collisions possible.

Une empreinte forte permet de reconnaître de manière très efficace un son, lui
étant presque unique. Mais une empreinte forte signifie aussi une 
reconnaissance plus difficile sur un enregistrement avec du bruit.

# Reconnaissance d'un son

## Stockage

Le stockage est un problème en lui même, un son pouvant contenir des milliers
de pics. Ce nombre peut varier mais aussi affectuer la qualité de la 
reconnaissance, comme décrit plus haut.

Une fonction de hachage peut ainsi être utilisée pour stocker les pics
souhaités, réduisant l'espace demandé.

## Alignement

Une empreinte du son enregistré est effectuée. L'enregistrement n'est
probablement cependant qu'une partie d'un son. Il faut donc pouvoir l'aligner
avec les données existantes.

Le couple temps/fréquence étant stocké mais l'enregistrement décalé, il n'est
possible de vérifier en premier lieu que la fréquence. Ensuite, la différence
entre le temps absolu de la base de données et celui de l'enregistrement est
calculé pour chaque pic.

Le son reconnu est alors celui présentant le plus de fois la même différence,
indiquant en effet que les pics d'amplitude enregistrés sont bien espacés.


# Répartition des charges

## Première soutenance

|                              | Brendan | Adrien | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:------:|:----:|
| Visualisation des fréquences | \kreuz  |        | \kreuz |      |
| Enregistrement d'un son      |         | \kreuz |        |\kreuz|
| Site Web                     | \kreuz  | \kreuz | \kreuz |\kreuz|


## Seconde soutenance

|                              | Brendan | Adrien | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:------:|:----:|
| Pics d'amplitude d'un son    |         | \kreuz | \kreuz |      |
| Empreinte d'un son           | \kreuz  |        |        |\kreuz|
| Site Web                     | \kreuz  | \kreuz | \kreuz |\kreuz|


## Soutenance finale

|                              | Brendan | Adrien | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:------:|:----:|
| Stockage des empreintes      |         |        | \kreuz |\kreuz|
| Alignement des échantillons  | \kreuz  | \kreuz |        |      |
| Reconnaissance finale        | \kreuz  | \kreuz | \kreuz |\kreuz|
| Site Web                     | \kreuz  | \kreuz | \kreuz |\kreuz|

\newpage

# Conclusion 

Un logiciel de reconnaissance de son peut nous apporter un nombre important de
connaissances. Non seulement au niveau de la partie physique d'un son et de
son traitement informatique, mais également au niveau des différents 
algorithmes impliqués dans la reconnaissance de celui-ci.

Ce projet s'annonce intéressant aussi bien dans sa dimension pédagogique que
ludique.
