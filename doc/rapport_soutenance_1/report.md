﻿# Introduction

Il arrive souvent de se demander, en écoutant la radio ou au bar, quelle est la
musique que l'on entend. Souvent, il suffit de demander aux personnes aux
alentours afin d'avoir une réponse plus ou moins correcte.

Mais voilà, ce système n'est pas fiable. Un ordinateur ferait-il mieux ?
C'est le but de notre projet : « le détecteur de casseroles ». Celui-ci a en 
effet pour but de reconnaître des musiques. Il est donc similaire
à l'application existante « Shazam ».

Le déroulement de ce projet s'étalera sur plusieurs mois et sera finalisé pour
la semaine du 23 mai 2016.

Voici notre progression pour la première soutenance.

\newpage

# Le groupe

## Mael "hatrix" Le Garrec

L'informatique m'attire depuis toujours. Mais c'est au cours de mes années à 
EPITA que j'ai commencé à découvrir ce qui m'intéresse vraiment.\
Mon centre d'intérêt principal aujourd'hui est en effet la programmation bas
niveau et toute la technicité qui en découle. Je compte en effet parmis mes
projets différents émulateurs applications système.

Le langage que je maitrise le mieux à ce jour est le Python. Il est en effet 
très utile pour réaliser nombre de scripts, que ce soit pour réaliser des 
programmes en eux-mêmes ou pour travailler facilement sur du bas niveau.\
Les CTFs du LSE m'intéressent beaucoup et c'est notamment grâce à ce langage
que j'ai pu un jour m'hisser dans le top10.

Mon projet de première année, non pas un jeu vidéo comme la plupart de mes
camarades, était un vrai challenge. Il était ici question d'un ordinateur, crée
à partir de la porte logique NAND. Je me suis occupé de la partie bas niveau :
création de l'ALU, du processeur puis de l'ordinateur complet au niveau 
logique. Je me suis également occupé de l'assembleur : la création des opcodes
nécessaires et utiles ainsi que le programme permettant à l'ordinateur émulé 
de l'exécuter.

L'an dernier, un OCR réalisé en C m'a permis de vraiment appronfondir ce 
langage, mal connu auparavant. Je ne l'utilisais en effet que très peu de par
sa difficulté et son manque de résultats rapides et faciles. Ce projet m'a 
permis de réaliser l'étendue de son potentiel et je l'utilise depuis 
régulièrement.

Le projet de cette année, faisant intervenir des analyses de son me permet de
me familiariser avec cet univers, encore méconnu de ma part. Il ne représente
en soi par une réelle difficulté d'un point de vue technique, mais surtout d'un
point de vue recherche, les notions requises n'étant pas triviales et non 
abordées en cours.

## Hélène Ksiezak

Contrairement à la grande majorité d'élèves d'epita ayant intégrée l'école car
passionés d'informatique, je dois admettre que dans mon cas, je
suis tombée ici par pur hasard. En effet, j'ai intégré cette école en sachant
faire un copier/coller et rien d'autre. Je n'ai fait ni la spé ISN en terminale,
ni de petit projet perso sur la calculatrice, de sites web: je ne me suis
pas du tout aventurée dans le monde de l'informatique. Cela peut paraitre
completement fou de décider sur un coup de tête d'intégrer une école
d'informatique mais je ne le regrette pas du tout. 
Certes, par rapport aux autres élèves j'ai un niveau très bas mais il ne faut
pas s'arrêter là!

Ainsi, en première année j'ai pu découvrir ce qu'était un peu plus
l'informatique en travaillant en groupe sur un projet de jeu vidéo. Et je dois
avouer que si j'ai appris une quantité de choses en participant au projet,
travailler en groupe m'a un peu dégouté.

Au commencement de notre deuxième année dans cette école, on nous a attribué la
tâche de réaliser une reconnaissance faciale. Si le sujet avait l'air aussi
effrayant qu'intéressant, c'est le travail en groupe qui m'a vraiment marqué: il
était inéxistant. Doté d'un piètre niveau comme le mien, ce fut donc très
difficile et fatigant d'essayer de réaliser un projet de ce niveau toute seule.
Et le résultat ne fut pas très étonnant: on avait pas du tout ce qui nous était
demandé. Cependant, je tiens à dire que l'expérience était unique car beaucoup
d'élèves d'autres groupes sont venus à mon secour et j'ai pu ainsi apprendre
énormément de choses.

Suite à ce qu'il s'était passé lors de cette soutenance, je ne vais pas dire que
j'ai été traumatisé par le terme “projet” mais on s'en rapproche assez. Ainsi,
lorsqu'il a fallut créer des nouveaux groupes pour le S4, j'ai tout de suite
cherché à être avec quelqu'un que je connaissais et qui était bon en code:
Brendan. Malheureusement, j'ai été vraiment déçue du sujet choisi mais les
autres membres du groupe étant tellement enchanté qu'il a fallut que je m'y
colle.

## Brendan "Exy" Harley

Passionné par l'informatique depuis le plus jeune âge, j'ai aujourd'hui une
expérience grandissante avec la programmation. Des premiers jeux vidéos à la
découverte de comment fonctionnent les programmes, pour enfin faire mes premiers
scripts en shell, j'ai toujours eu envie de découvrir plus. Depuis mon entrée a Epita, j'ai eu
l'occasion de manipuler de nombreux aspects de la programmation, tant du coté
orienté objet que fonctionnel.

L'année dernière j'ai ainsi eu l'occasion de développer un jeu video à l'aide
du moteur de jeu video Unity. J'ai pu comprendre grâce à ce projet la mise en
place d'un réseau, d'une interface graphique et de gestion d'un projett d'une
taille assez conséquente. Lors de mon troisième semestre, notre projet fût une
reconnaissance faciale. Celle-ci se découpa en deux temps, tout d'abord nous
devions détecter s'il y avait un visage au sein de l'image, pour enfin pouvoir
l'identifier. La detection s'effectua à l'aide de l'algorithme de Viola et
Jones, l'identification à l'aide de la méthode dite des "eigenfaces".

Aujourd'hui j'ai la chance de pouvoir utiliser ces compétences pour un projet qui
me semble intéressant. Ce projet est pour moi une occasion de découvrir un nouvel élément
de travail : le son.

\newpage

# Répartition des tâches

Ayant perdu un membre de projet au cours de l'année, la répartition des tâches
a été changée. Nous avons en effet réalisé que l'organisation de notre projet 
se devait d'être différente.\
La répartition actuelle des tâches est ainsi la suivante :

|                              | Brendan | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:----:|
| Transformation de Fourier    | \kreuz  |        |      |
| Visualisation de fréquence   |         |        |\kreuz|
| Enregistrement d'un son      |         | \kreuz |      |
| Site Web                     | \kreuz  | \kreuz |\kreuz|

La recherche constitue une part importante de chacune de ces tâches.

\newpage

# Analyse d'un Son
## L'enregistrement


Le programme réalisé permet d'ouvrir une interface audio pour la saisie d'un
son, et le configure pour supporter la stéréo, soit 16 bit , 44.1kHz.La saisie
renvoie ici au processus qui permet l'obtention d'un signal provenant de
l'exterieur de l'ordinateur. Une application récurrente à la saisie audio est
l'enregistrement (comme celui de l'input d'un microphone à un fichier son).
Cependant, la saisie d'un son ne concorde pas avec le fait d'enregistrer car
enregistrer implique que l'application sauvegarde toujours les données de son
recueillie. Une application saisissant un son ne signifie donc pas
obligatoirement qu'elle va le stocker.
Pour ce faire, on a utilisé la librairie d'asoundlib. On a d'abord vérifié que
l'on puisse se connecter a la carte audio de l'ordinateur et ensuite on a
verifié que la description du format du fichier puisse être générée (access
type, sample format, sample rate...).


## Transformation de Fourier

La transformée de Fourier est une somme de fonctions trigonométriques des
différentes fréquences composant notre son. Son application nous permet donc, à
l'aide d'une implémentation efficace, de récuperer facilement les fréquences
principales; ce qui nous sera utile pour son analyse plus tard.

![Transformation de Fourier](./images/dft.jpg)

La définition formelle de la tranformation de Fourier est une notion complexe à
implementer :

![Definition Formelle](./images/notfast.png)

Nous avons donc décidé d'utiliser la transformation de Fourier rapide (FFT) pour
plusieures raison. Premièrement, il s'agit d'une approche facilitant grandement
l'implémentation d'une Transformation de Fourier Discrete (TFD). De plus, elle
offre une complexité O(n log n), alors que l'algorithme de base offre une
complexité O(n²).

![Transformation de Fourier Rapide](./images/fast.png)

## Algorithme de Cooley-Tukey

Nous avons donc besoin d'implementer une Transformation de Fourier, pour cela
nous avons décidé d'utiliser l'algorithme de Coley-Tukey.
Cet algorithme définit une Transformation de Fourier discrete
(tfd) en une somme de tfd, ce qui a pour effet d'améliorer la complexité, tout
en offrant des possibilités d'optimisation. L'algorithme se base sur le principe
de "Divide and Conquer", on coupe le tableau des entrées en 2 a chaque
itération, quand on obtient un tableau assez petit, on effectue une tfd dessus.

![Principe d'un algorithme "Divide and Conquer"](./images/divide.png)



\newpage

# Analyse des données

## Fichiers WAV

Les fichiers WAV sont à la musique ce que sont les fichiers BMP à l'image : 
l'exemple le plus simple à notre disposition.

| Octets | Exemple | Description                        |
| ------ | ------  | -----------                        |
| 4      | "RIFF"  | RIFF Spécification                 |
| 4      | x       | Taille du fichier                  |
| 4      | "WAVE"  | Fichier WAVE                       |
| 4      | "fmt"   | Début du chunk format              |
| 4      | 16      | Taille du chunk format             |
| 2      | 1       | Type du format                     |
| 2      | 2       | Nombre de canaux                   |
| 4      | 44100   | SampleRate                         |
| 4      | 176400  | ByteRate : SR * BpS * Channels / 8 |
| 2      | 4       | Octets par sample                  | 
| 2      | 16      | Bits par sample                    |
| 4      | "data"  | Début du chunk data                |
| 4      | x       | Taille du chunk data               |
| x      | data    | Données                            |

Les données représentent le changement de l'intensité de la tension du signal
audio au cours du temps. Non compressé, ce signal est donc facilement 
traitable.

\newpage

## Adaptation des données

De par sa nature, la transformation rapide de Fourier requiert des nombres
complexes en entrée. Les fichiers WAV ne comportant que des réels, il nous
faut pouvoir adapter ces données.

Une conversion en complexes est donc effectuée pour chaque sample du chunk
de données, permettant d'effectuer une FFT dessus.

Chaque sample contient les données de tous les canaux décrits dans le header.
Pour n'analyser qu'un canal il ne faut donc que lire les données qui nous 
intéressent. La plupart du temps, lire deux à deux les données des canaux donne
les résultats attendus, le son étant stéreo.

![Chunks du format WAV](./images/format_wav.png)

\newpage

## Recherche d'amplitude

Une transformation de Fourier rapide sur nos données nous permet d'obtenir des
nombres complexes. Cette transformation doit s'effectuer sur des fenêtres, un
intervalle arbitraire de données pour pouvoir fonctionner correctement.\
En effet, si la fenêtre est trop petite, la fréquence ne peut être trouvée de
manière sûre.

Une FFT est donc effectuée sur notre intervalle. La liste de complexes en
résultat nous permet de trouver les amplitudes et donc la note fondamentale
d'une partie d'un son.\
Cette amplitude se calcule via la norme du complexe :

\begin{center}
  $amplitude = sqrt(re^2 + im^2)$
\end{center}

On obtient ainsi une liste d'amplitude pour chaque complexe. L'amplitude 
maximale définit notre fondamentale. Les harmoniques ne nous sont pas utiles.

![Fondamentale](images/fondamentale.png)

\newpage

## Recherche de fréquence

La recherche de fréquence est elle aussi aisée. Après avoir trouvé l'amplitude
maximale de notre fenêtre, il est possible de trouver la fréquence qui lui
correspond. Les fréquences se suivent en effet et il est possible de déduire
celle qui nous intéresse.

les fréquences sont divisées selon le *sample rate* et la taille de la fenêtre.
On peut ainsi diviser les fréquences via cette formule :

\begin{center}
  $frequences = n * Fs / N$
\end{center}

où *frequences* représente l'intervalle recherché, *n* la position de la valeur
dans la liste, *Fs* le *samplerate* et *N* la taille de la fenêtre.

Par exemple, pour un samplerate de 44100Hz, une fenêtre de 4096 et un *n* 
trouvé à 93, on trouve 1001Hz pour un son de 1kHz. Cette approximation est 
inévitable de par la nature de la FFT.

\newpage

## Difficultés 

La grande difficulté de cette partie ne réside pas dans sa technicité. Elle
est en effet plutôt présente dans la compréhension des outils à notre 
disposition.

Un jugement trop hatif et commun nous a laissé penser à une tâche facile à 
effectuer. Hors, la compéhension du fonctionnement des entrées et sorties de la
FFT ainsi que de la réelle signification des données d'un fichier audio ne sont
pas triviales.

Ce sont donc les majeures difficultés rencontrées : l'apprentissage de notions
totalement inconnues. Celles-ci sont toutefois très intéressantes et permettent
de mieux comprendre la structure d'un son et tous les problèmes liés à sa
représentation non pas analogue mais numérique.

\newpage

# Divers

## Site Web

Afin de ne pas perdre de temps sur l'élaboration d'un site web, nous avons
opté pour Pelican\footnote{http://docs.getpelican.com/}, un générateur de site
statique écrit en python. Celui-ci nous permet de nous focaliser sur l'écriture
d'articles et non sur l'HTML. Un modèle de template crée par nos soins aurait
été envisageable mais n'est pas le but initial du projet.

Pelican nous permet en effet d'écrire nos articles sous forme de fichiers 
*markdown* ou *reStructuredText*, rendant leur écriture beaucoup plus aisée.
L'intégration d'images se fait elle-aussi facilement, ne détournant pas notre 
attention sur la gestion de la mise en page.

De plus, de nombreux thèmes existent et sont disponibles gratuitement afin
d'avoir un site configurable à souhait et répondant à nos attentes esthétiques.

Notre site est ainsi disponible à cette adresse :
\vspace{-.7cm}
\begin{center}
 \url{http://casseroles.hatrix.fr}
\end{center}

## Canal IRC

Un canal IRC a été mis en place pour rendre plus aisée la communication entre
les membres du groupe. Ce dernier présent sur rezosup, réseau utilisé par 
nombre d'écoles d'informatique, nous permet d'être en contact constant avec
d'autres membres de l'EPITA lors du développement du projet.\
Notre canal est ainsi accessible sur *#casseroles* à l'adresse 
*irc.rezosup.org/6667*

\newpage

# Tâches restantes à effectuer

La transformation de Fourier étant traitée de la détermination d'une fréquence
effectuée, il reste à créer l'empreinte d'un son. Celle-ci doit néanmoins être
précédée d'une meilleur compréhension du fenêtrage et des fonctions associées
pour obtenir un ensemble de données cohérent et utilisable de manière optimale.

À terme, les empreintes serviront donc à reconnaître un son enregistré via 
l'alignement des échantillons.

# Seconde soutenance
|                              | Brendan | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:----:|
| Empreinte d'un son           | \kreuz  | \kreuz |\kreuz|
| Site Web                     | \kreuz  | \kreuz |\kreuz|


## Soutenance finale

|                              | Brendan | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:----:|
| Stockage des empreintes      |         | \kreuz |\kreuz|
| Alignement des échantillons  | \kreuz  |        |      |
| Reconnaissance finale        | \kreuz  | \kreuz |\kreuz|
| Site Web                     | \kreuz  | \kreuz |\kreuz|


\newpage

# Conclusion 

Pour conclure, nous avons atteind les objectifs prévus pour cette soutenance.
Nous attendons donc avec impatience la suivante afin de pouvoir entrer dans le
vif du sujet : la fabrication de l'empreinte d'un son !




