\lhead{Plan de soutenance}
\rhead{Détecteur de casserole}

\vspace{-10cm}

# Introduction

# Enregistrement micro

# Transformation de Fourier

# Traitement du signal

## Conversion mono

## Downsampling

## Filtre passe-bas

## Fenêtrage

## Fréquences

# Empreinte

## Target zones

## Adresse d'un point

## Base de données

# Reconnaissance

## Traitement

## Recherche des adresses

## Cohérence des temps

## Reconnaissance finale

# Conclusion
