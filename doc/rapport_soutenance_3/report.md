﻿# Introduction

Il arrive souvent de se demander, en écoutant la radio ou au bar, quelle est la
musique que l'on entend. Souvent, il suffit de demander aux personnes aux
alentours afin d'avoir une réponse plus ou moins correcte.

Mais voilà, ce système n'est pas fiable. Un ordinateur ferait-il mieux ?
C'est le but de notre projet : « le détecteur de casseroles ». Celui-ci a en 
effet pour but de reconnaître des musiques. Il est donc similaire
à l'application existante « Shazam ».

Le déroulement de ce projet s'étalera sur plusieurs mois et sera finalisé pour
la semaine du 23 mai 2016.

Ce rapport fait suite à celui de la première soutenance. Voici ainsi la
progression de notre projet pour la seconde soutenance.

\newpage

# Le groupe

## Mael "hatrix" Le Garrec

L'informatique m'attire depuis toujours. Mais c'est au cours de mes années à 
EPITA que j'ai commencé à découvrir ce qui m'intéresse vraiment.\
Mon centre d'intérêt principal aujourd'hui est en effet la programmation bas
niveau et toute la technicité qui en découle. Je compte en effet parmis mes
projets différents émulateurs applications système.

Le langage que je maitrise le mieux à ce jour est le Python. Il est en effet 
très utile pour réaliser nombre de scripts, que ce soit pour réaliser des 
programmes en eux-mêmes ou pour travailler facilement sur du bas niveau.\
Les CTFs du LSE m'intéressent beaucoup et c'est notamment grâce à ce langage
que j'ai pu un jour m'hisser dans le top10.

Mon projet de première année, non pas un jeu vidéo comme la plupart de mes
camarades, était un vrai challenge. Il était ici question d'un ordinateur, crée
à partir de la porte logique NAND. Je me suis occupé de la partie bas niveau :
création de l'ALU, du processeur puis de l'ordinateur complet au niveau 
logique. Je me suis également occupé de l'assembleur : la création des opcodes
nécessaires et utiles ainsi que le programme permettant à l'ordinateur émulé 
de l'exécuter.

L'an dernier, un OCR réalisé en C m'a permis de vraiment appronfondir ce 
langage, mal connu auparavant. Je ne l'utilisais en effet que très peu de par
sa difficulté et son manque de résultats rapides et faciles. Ce projet m'a 
permis de réaliser l'étendue de son potentiel et je l'utilise depuis 
régulièrement.

Le projet de cette année, faisant intervenir des analyses de son me permet de
me familiariser avec cet univers, encore méconnu de ma part. Il ne représente
en soi par une réelle difficulté d'un point de vue technique, mais surtout d'un
point de vue recherche, les notions requises n'étant pas triviales et non 
abordées en cours.

## Hélène Ksiezak

Contrairement à la grande majorité d'élèves d'epita ayant intégrée l'école car
passionés d'informatique, je dois admettre que dans mon cas, je
suis tombée ici par pur hasard. En effet, j'ai intégré cette école en sachant
faire un copier/coller et rien d'autre. Je n'ai fait ni la spé ISN en terminale,
ni de petit projet perso sur la calculatrice, de sites web: je ne me suis
pas du tout aventurée dans le monde de l'informatique. Cela peut paraitre
completement fou de décider sur un coup de tête d'intégrer une école
d'informatique mais je ne le regrette pas du tout. 
Certes, par rapport aux autres élèves j'ai un niveau très bas mais il ne faut
pas s'arrêter là!

Ainsi, en première année j'ai pu découvrir ce qu'était un peu plus
l'informatique en travaillant en groupe sur un projet de jeu vidéo. Et je dois
avouer que si j'ai appris une quantité de choses en participant au projet,
travailler en groupe m'a un peu dégouté.

Au commencement de notre deuxième année dans cette école, on nous a attribué la
tâche de réaliser une reconnaissance faciale. Si le sujet avait l'air aussi
effrayant qu'intéressant, c'est le travail en groupe qui m'a vraiment marqué: il
était inéxistant. Doté d'un piètre niveau comme le mien, ce fut donc très
difficile et fatigant d'essayer de réaliser un projet de ce niveau toute seule.
Et le résultat ne fut pas très étonnant: on avait pas du tout ce qui nous était
demandé. Cependant, je tiens à dire que l'expérience était unique car beaucoup
d'élèves d'autres groupes sont venus à mon secour et j'ai pu ainsi apprendre
énormément de choses.

Suite à ce qu'il s'était passé lors de cette soutenance, je ne vais pas dire que
j'ai été traumatisé par le terme “projet” mais on s'en rapproche assez. Ainsi,
lorsqu'il a fallut créer des nouveaux groupes pour le S4, j'ai tout de suite
cherché à être avec quelqu'un que je connaissais et qui était bon en code:
Brendan. Malheureusement, j'ai été vraiment déçue du sujet choisi mais les
autres membres du groupe étant tellement enchanté qu'il a fallut que je m'y
colle.

## Brendan "Exy" Harley

Passionné par l'informatique depuis le plus jeune âge, j'ai aujourd'hui une
expérience grandissante avec la programmation. Des premiers jeux vidéos à la
découverte de comment fonctionnent les programmes, pour enfin faire mes
premiers scripts en shell, j'ai toujours eu envie de découvrir plus. Depuis mon
entrée a Epita, j'ai eu l'occasion de manipuler de nombreux aspects de la
programmation, tant du coté orienté objet que fonctionnel.

L'année dernière j'ai ainsi eu l'occasion de développer un jeu video à l'aide
du moteur de jeu video Unity. J'ai pu comprendre grâce à ce projet la mise en
place d'un réseau, d'une interface graphique et de gestion d'un projett d'une
taille assez conséquente. Lors de mon troisième semestre, notre projet fût une
reconnaissance faciale. Celle-ci se découpa en deux temps, tout d'abord nous
devions détecter s'il y avait un visage au sein de l'image, pour enfin pouvoir
l'identifier. La detection s'effectua à l'aide de l'algorithme de Viola et
Jones, l'identification à l'aide de la méthode dite des "eigenfaces".

Aujourd'hui j'ai la chance de pouvoir utiliser ces compétences pour un projet
qui me semble intéressant. Ce projet est pour moi une occasion de découvrir un
nouvel élément de travail : le son.

\newpage

# Répartition des tâches

La répartition de notre projet fut la suivante.

|                              | Brendan | Hélène | Maël |
|:----------------------------:|:-------:|:------:|:----:|
| Enregistrement micro         |         | \kreuz |      |
| Transformation de Fourier    | \kreuz  |        |      |
| Visualisation de fréquence   | \kreuz  |        |      |
| Traitement du signal audio   |         |        |\kreuz|
| Empreinte d'un son           |         |        |\kreuz|
| Reconnaissance               |         |        |\kreuz|
| Site Web                     | \kreuz  | \kreuz |\kreuz|

La recherche constitue une part importante de chacune de ces tâches.

\newpage

# Analyse d'un Son
## L'enregistrement

Le programme réalisé permet d'ouvrir une interface audio pour la saisie d'un
son, et le configure pour supporter la stéréo, soit 16 bit , 44.1kHz.
Lors de la première soutenance, nous avions réussit l'enregistrement d'un son
pour une durée de 5 secondes. Cependant, le format obtenu au final était un
format raw et nous voulions un format wav.
Pour cette deuxième soutenance, notre travail a donc été d'obtenir le format
que nous voulions: le wav. La différence entre ces deux formats est que le
raw est un format audio brut, sans aucune compression, ne contenant donc 
aucune information d'en-tête: de métadonnées. Cela sous-entend donc qu'il n'y
a aucune information sur le sample rate, le nombre de canaux...
Nous avons donc créé un header permettant l'obtention de ces informations lors
de l'enregistrement. Ainsi, cela nous a permis d'obtenir un son au format wav.


## Transformation de Fourier

La transformée de Fourier est une somme de fonctions trigonométriques des
différentes fréquences composant notre son. Son application nous permet donc, à
l'aide d'une implémentation efficace, de récuperer facilement les fréquences
principales; ce qui nous sera utile pour son analyse plus tard.

![Transformation de Fourier](./images/dft.jpg)

La définition formelle de la tranformation de Fourier est une notion complexe à
implementer :

![Definition Formelle](./images/notfast.png)

Nous avons donc décidé d'utiliser la transformation de Fourier rapide (FFT) pour
plusieures raison. Premièrement, il s'agit d'une approche facilitant grandement
l'implémentation d'une Transformation de Fourier Discrete (TFD). De plus, elle
offre une complexité O(n log n), alors que l'algorithme de base offre une
complexité O(n²).

![Transformation de Fourier Rapide](./images/fast.png)

## Algorithme de Cooley-Tukey

Nous avons donc besoin d'implementer une Transformation de Fourier, pour cela
nous avons décidé d'utiliser l'algorithme de Coley-Tukey.
Cet algorithme définit une Transformation de Fourier discrete
(tfd) en une somme de tfd, ce qui a pour effet d'améliorer la complexité, tout
en offrant des possibilités d'optimisation. L'algorithme se base sur le principe
de "Divide and Conquer", on coupe le tableau des entrées en 2 a chaque
itération, quand on obtient un tableau assez petit, on effectue une tfd dessus.

![Principe d'un algorithme "Divide and Conquer"](./images/divide.png)



\newpage

# Analyse des données

## Fichiers WAV

Les fichiers WAV sont à la musique ce que sont les fichiers BMP à l'image : 
l'exemple le plus simple à notre disposition.

| Octets | Exemple | Description                        |
| ------ | ------  | -----------                        |
| 4      | "RIFF"  | RIFF Spécification                 |
| 4      | x       | Taille du fichier                  |
| 4      | "WAVE"  | Fichier WAVE                       |
| 4      | "fmt"   | Début du chunk format              |
| 4      | 16      | Taille du chunk format             |
| 2      | 1       | Type du format                     |
| 2      | 2       | Nombre de canaux                   |
| 4      | 44100   | SampleRate                         |
| 4      | 176400  | ByteRate : SR * BpS * Channels / 8 |
| 2      | 4       | Octets par sample                  | 
| 2      | 16      | Bits par sample                    |
| 4      | "data"  | Début du chunk data                |
| 4      | x       | Taille du chunk data               |
| x      | data    | Données                            |

Les données représentent le changement de l'intensité de la tension du signal
audio au cours du temps. Non compressé, ce signal est donc facilement 
traitable.

\newpage

## Adaptation des données

### Stéréo vers mono

Afin de simplifier le traitement des données, les sons sont convertis de stéréo
vers mono. La technique utilisée est simple : prendre deux à deux les données
de chaque chunk et en faire la moyenne.

Le son résultant garde donc le même samplerate, seul son nombre de canaux
change.

![Chunks du format WAV](./images/format_wav.png)


\newpage

## Downsampling

Le downsampling est une technique visant à réduire la fréquence 
d'échantillonage d'un son. La faisant passer de 44100Hz à 11025Hz permet 
d'économiser en temps de calcul. La précision reste cependant la même tout en
travaillant sur un nombre réduit de données.

En réduisant la fréquence d'échantillonage, certaines fréquences ne seront plus
audibles. Le théorème de Shannon-Nyquist indique en effet qu'il faut avoir une
fréquence d'échantillonage du double de la fréquence maximale d'un son.

Avec une fréquence d'échantillonage de 11025Hz, les fréquences au dela de 5kHz
ne seront plus accessibles. Cela ne pose pas de problème, ces fréquences étant
trop hautes pour êtes significatives dans une musique.

![Théorème de SHannon-Nyquist](./images/shannon.png)

\newpage

## Filtre passe-bas

Le downsampling permet donc de réduire la fréquence d'échantillonage et ainsi
de gagner en rapidité de calcul. Cette technique doit cependant être précédée
d'un filtre passe-bas pour éviter l'aliasing.

Comme défini précédemment, le théorème de Shannon-Nyquist nous indique que des
fréquences au dela de 5kHz ne pourront être détectées. Les données concernant
ces fréquences sont cependant toujours présentes et un repliement de spectre
peut apparaître. Une fréquence élevée va ainsi être traîtée comme une fréquence
moinde.

![Rempliement de spectre](./images/repliement.png)

Le filtre passe-bas nous permet donc via une fonction de transfert classique
d'atténuer les fréquences trop élevées. Ici, la fréquence de coupure est égale
à 5kHz.

\newpage

## Fenêtrage

Avant de pouvoir appliquer Fourier, il faut appliquer une fonction de 
fenêtrage. Sans celle-ci, des fréquences parasites peuvent apparaître et 
fausser les résultats. L'analyse de signaux en temps limité est en effet 
problématique et ne permet pas d'être totalement précis.

Différentes fonctions de fenêtrage existent et sont utiles dans différents 
cas : selon les fréquences, les amplitudes, le bruit, etc. Pour avoir un bon
compromis, nous utilisons la fonction de Hamming.

![Fonctions de fenêtrage](./images/window.png)

\newpage 

## Recherche d'amplitude

Une transformation de Fourier rapide sur nos données nous permet d'obtenir des
nombres complexes. Cette transformation doit s'effectuer sur des fenêtres, un
intervalle arbitraire de données pour pouvoir fonctionner correctement.\
En effet, si la fenêtre est trop petite, la fréquence ne peut être trouvée de
manière sûre.

Une FFT est donc effectuée sur notre intervalle. La liste de complexes en
résultat nous permet de trouver les amplitudes et donc la note fondamentale
d'une partie d'un son.\
Cette amplitude se calcule via la norme du complexe :

\begin{center}
  $amplitude = sqrt(re^2 + im^2)$
\end{center}

On obtient ainsi une liste d'amplitude pour chaque complexe. L'amplitude 
maximale définit notre fondamentale. Les autre d'amplitude suffisament élevées 
peuvent également nous être utile.

![Fondamentale](images/fondamentale.png)

\newpage

## Recherche de fréquence

La recherche de fréquence est elle aussi aisée. Après avoir trouvé et
sélectionné les amplitudes de notre fenêtre, il est possible de trouver les 
fréquences correspondantes. Les fréquences se suivent en effet et il est
possible de déduire celles qui nous intéressent.

les fréquences sont divisées selon le *sample rate* et la taille de la fenêtre.
On peut ainsi diviser les fréquences via cette formule :

\begin{center}
  $frequences = n * Fs / N$
\end{center}

où *frequences* représente l'intervalle recherché, *n* la position de la valeur
dans la liste, *Fs* le *samplerate* et *N* la taille de la fenêtre.

Par exemple, pour un samplerate de 44100Hz, une fenêtre de 4096 et un *n* 
trouvé à 93, on trouve 1001Hz pour un son de 1kHz. Cette approximation est 
inévitable de par la nature de la FFT.

Nous travaillons sur un samplerate de 11025Hz, une fenêtre de 1024 est ainsi
suffisante pour obtenir la même précision qu'une fenêtre de 4096 avec un
samplerate de 44100Hz.

\newpage

# Empreinte d'un fichier

Pour obtenir l'empreinte d'un fichier, une technique simple serait 
d'enregistrer des couples temps-fréquence. Cette technique a toutefois ses 
limites, le temps de calcul pouvant être très long. Cette technique, en plus
d'être longue, fournirait un nombre élevé de faux positifs, qui devront donc
être éliminés.

Une autre technique est alors employée, utilisant des _target zones_.
Après avoir récupéré les couples temps-fréquences d'amplitude minimale, on peut
alors les traiter.

\newpage 

## Target zones

Une target zone est un regroupement de 5 points. Une target zone est également
liée à un point d'ancrage, défini comme étant celui 3 points derrière le 
première de la zone.

![Deux target zones](./images/target.jpg)

Un nouveau point est ainsi constitué de sa propre fréquence, de la fréquence de
l'ancre mais également du temps séparant ces deux points.
Ces trois données sont codées sur un entier de 32 bits. Ceci nous permet de
faire des recherches rapidement, ces trois informations constituant une 
adresse.

À chaque adresse est alors associée une liste constenant l'indice temporel 
absolu de l'ancre ainsi que l'ID de la musique à laquelle ces informations 
appartiennent.

## Adresse d'un point

Un point est donc défini par trois composantes : sa fréquence, la fréquence de
l'ancre ainsi que le temps les séparant. Codées sur un entier 32 bits, ces
informations constituent l'adresse de notre point.

Adresse d'un point : 
\begin{center}
  $[freq(point), freq(ancre), delta(t(p) - t(a))]$
\end{center}

À chaque point est donc associé un couple :

\begin{center}
  $[t(a), song ID]$
\end{center}

L'indice temporel absolu de l'ancre nous sera utile plus tard pour la 
reconnaissance. L'ID de la musique nous permet bien évidemment de faire le lien
avec son nom et celui de l'artiste.

Une adresse peut toutefois renvoyer plusieurs couples, il n'est pas rare que 
cela se produise.


## Base de données

Pour plus de facilité, les données sont enregistrées dans une base SQL. Seuls
trois champs sont nécessaires pour chaque point : l'adresse, l'indice de temps
de l'ancre ainsi que l'ID du son.

De cette manière, les recherches seront rapides. Une adresse peut en effet se
trouver en O(1) et renverra une liste de couples ancre/son. Ceux-ci serviront
à identifier un son via les proportions reçues.

Ce système permet d'économiser en espace de stockage. Après différents tests,
une empreinte moyenne prend 1.08Mo d'espace disque contre environ 5Mo pour un 
mp3.

\newpage

## Difficultés 

La grande difficulté de cette partie ne réside pas dans sa technicité. Elle
est en effet plutôt présente dans la compréhension des outils à notre 
disposition.

Après avoir lu et relu différents articles, il devient aisé de comprendre les
spécificités de chaque algorithme. 

Il reste toutefois à trouver les coefficients pour calibrer correctement notre
détection. Il faut en effet rendre nos algorithmes résistants au bruit tout en
gardant un bon seuil de précision.

\newpage

# Reconnaissance

## Traitement initial

Comme pour l'ajout d'une empreinte dans la base de donnée, il faut traiter le
son à reconnaître. Celui-ci subit les mêmes étapes : conversion en mono, filtre
passe-bas, downsampling, fonction de fenêtrage, fft, génération de l'empreinte.

Puis vient la reconnaissance en elle-même.

## Recherche des adresses

Une fois l'empreinte de l'extrait générée, on peut rechercher les adresses dans
la base de données. Chaque adresse est ainsi comparée à celles de la base de 
données, on obtient des couples $[t(a), song ID]$.

On pourrait, rien qu'avec cette recherche, déterminer de quelle musique 
provient l'extrait. Or, cette méthode n'est pas fiable. Il se peut en effet que
les adresses trouvées ne se suivent pas dans la musique.

![Mauvaise cohérence des temps](./images/search-min.jpg)

On peut donc avoir des faux positifs avec cette méthode. Il faut continuer à
traiter nos couples.

## Cohérence des temps

Comme indiqué précédement, notre recherche nous renvoie une composante 
intéressante : l'indice temporel de l'ancre. Comme nous possédons le delta 
temporel entre l'ancre et le point dans l'adresse, on peut retrouver le 
décalage absolu de notre point avec la musique d'origine.

On calcule ainsi tous les décalages pour chaque musique retournée. On utilise
ensuite une hashmap pour compter les occurences de chaque décalage, pour chaque
musique.

On choisit par la suite le décalage avec le maximum d'occurence, pour chaque 
musique.

La musique possédant un nombre maximum est notre première candidate pour la 
reconnaissance. En effet, si le même décalage revient souvent, il y a fort à 
parier que cette musique est la bonne. Ceci peut ne pas marcher si la musique
est jouée à une vitesse différente de celle enregistrée.

## Efficacité

Pour une base de données de 700 musiques et d'environ 760Mo, il reste rapide
d'effectuer une recherche.
Certaines musiques d'à peine une demie seconde (!) peuvent être trouvées en 
moins d'une seconde de calculs !

En moyenne, le taux d'erreur devient très faible pour une musique de
secondes. Le temps de calcul se situe alors aux alentours des 10 secondes.

Un moyen simple d'améliorer ce temps de calcul serait de multi-threader les
caculs et recherches. Peut-être même de changer le système de base de données.

![Exemple de reconnaissance](./images/recog.jpg)

\newpage

# Amélioration possibles

Pour améliorer le projet, différentes solutions se présentent à nous :

* Stocker non pas deux points dans l'adresse mais trois
* Améliorer le système de base de donnée
* Multi-threader les calculs

Sans ces améliorations, le projet fonctionne tout de même rapidement.

\newpage

# Divers

## Site Web

Afin de ne pas perdre de temps sur l'élaboration d'un site web, nous avons
opté pour Pelican\footnote{http://docs.getpelican.com/}, un générateur de site
statique écrit en python. Celui-ci nous permet de nous focaliser sur l'écriture
d'articles et non sur l'HTML. Un modèle de template crée par nos soins aurait
été envisageable mais n'est pas le but initial du projet.

Pelican nous permet en effet d'écrire nos articles sous forme de fichiers 
*markdown* ou *reStructuredText*, rendant leur écriture beaucoup plus aisée.
L'intégration d'images se fait elle-aussi facilement, ne détournant pas notre 
attention sur la gestion de la mise en page.

De plus, de nombreux thèmes existent et sont disponibles gratuitement afin
d'avoir un site configurable à souhait et répondant à nos attentes esthétiques.

Notre site est ainsi disponible à cette adresse :
\vspace{-.7cm}
\begin{center}
 \url{http://casseroles.hatrix.fr}
\end{center}

## Canal IRC

Un canal IRC a été mis en place pour rendre plus aisée la communication entre
les membres du groupe. Ce dernier présent sur rezosup, réseau utilisé par 
nombre d'écoles d'informatique, nous permet d'être en contact constant avec
d'autres membres de l'EPITA lors du développement du projet.\
Notre canal est ainsi accessible sur *#casseroles* à l'adresse 
*irc.rezosup.org/6667*


\newpage

# Conclusion 

Nous avons fini ce projet non seulement dans les temps mais tout simplement 
fini !
La reconnaissance marche en effet de manière efficace, même avec une large base
de données.

De nombreuses difficultés ont été rencontrées tout au long de ce projet. 
Principalement de l'incompréhension quant aux outils à notre disposition et 
aux concepts à assimiler.
Mais ces difficultés ont toutes été surmontées. Nous avons appris beaucoup avec
ce projet qui nous semblait simple au début mais qui s'ets révélé plus 
compliqué que prévu.
