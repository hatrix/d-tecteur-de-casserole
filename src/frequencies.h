#include <complex.h>

double magnitude(complex double c);
void hamming(float *buf, int nb_windows, int n);
int *frequency(complex double *buf_c, int *len);
float *lowpass(float* buf, int cutoff, int samplerate, int length);

struct tf_point // time-frequency point
{
  int frequency; // index of frequency on a 512 array
  int time;
};
