#include <stdlib.h>
#include <stdio.h>
#include <sqlite3.h>
#include <stdint.h>
#include <string.h>
#include "target.h"

static int callback_id(void *data, int argc, char **argv, char **azColName)
{
  (void)argv;
  (void)azColName;

  if(argc > 0)
    *(int*)data = atoi(argv[0]);
  else
    *(int*)data = 0;

  return 0;
}

static int callback_exists(void *data, int argc, char **argv, char **azColName)
{
  (void)argv;
  (void)azColName;

  if(argc > 1)
    *(int*)data = 1;
  else
    *(int*)data = 0;

  return 0;
}

static int callback_address(void *data, int argc, char **argv, char **azColName)
{
  (void)azColName;
  for(int i = 0; i < argc; i++)
  {
    //printf("%s | %s\n", azColName[i], argv[i]);
  }

  char *ptr;
  *(uint64_t*)data = strtoul(argv[2], &ptr, 10);

  return 0;
}

static int callback_song(void *data, int argc, char **argv, char **azColName)
{
  (void)argc;
  (void)azColName;
  (void)data;
  printf("  - Artist: %s\n", argv[2]);
  printf("  - Song: %s\n", argv[1]);
  
  return 0;
}

int create_db(char *name)
{
  sqlite3 *db;
  int rc;

  rc = sqlite3_open(name, &db);
  if(rc)
    return 1;

  sqlite3_close(db);
  
  return 0;
}

int exec_sql(char *name, char* sql, char **msg)
{
  sqlite3 *db;
  char *zErrMsg = 0;
  int rc;
  
  rc = sqlite3_open(name, &db);
  
  rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
  if(rc != SQLITE_OK)
  {
    *msg = zErrMsg;
    return 1;
  }
  sqlite3_close(db);

  return 0;
}

int return_song(char *name, int ID, char **artist, char **sname)
{
  (void)artist;
  (void)sname;

  char *base = "SELECT * FROM SONGS WHERE ID = '%d';";
  int l;
  char *new;
  sqlite3 *db;
  char *msg;

  sqlite3_open(name, &db);
  
  l = snprintf(NULL, 0, base, ID);
  new = malloc(sizeof(char) * (l + 1));

  sprintf(new, base, ID);

  sqlite3_exec(db, new, callback_song, NULL, &msg);
  free(new);

  sqlite3_close(db);

  return 0;
}

int check_address(sqlite3 *db, int *rc, uint32_t fdt, uint64_t tID, int **count, int number_IDs, int *lens)
{
  (void)rc;
  (void)number_IDs;
  (void)callback_address;

  sqlite3_stmt *stmt;
  int row = 0;
  
  char *base = "SELECT * FROM FINGERPRINTS WHERE ADDRESS='%u';";

  int l;
  char *new;
  l = snprintf(NULL, 0, base, fdt);
  new = malloc(sizeof(char) * (l + 1));
  sprintf(new, base, fdt);

  sqlite3_prepare_v2(db, new, strlen(new) + 1, &stmt, NULL);

  uint64_t offset_record = (tID >> 32) + (fdt & 0xfff);
  while (1) {
    int s;

    s = sqlite3_step(stmt);
    if(s == SQLITE_ROW) 
    {
      const unsigned char *offset;
      const unsigned char *songID;
      offset = sqlite3_column_text(stmt, 1);
      songID = sqlite3_column_text(stmt, 2);
      //printf ("%d: %s | %s\n", row, offset, songID);

      int ID = atoi((const char*)songID);
      lens[ID] += 1;
      count[ID] = realloc(count[ID], sizeof(int*) * lens[ID]+1);
      count[ID][lens[ID]] = atoi((const char*)offset) - offset_record;

      //printf("%s | %lu | %d\n", offset, offset_record, count[ID][lens[ID]]);

      row++;
    }
    else if(s == SQLITE_DONE) 
    {
      break;
    }
    else 
    {
      fprintf (stderr, "Failed.\n");
      exit (1);
    }
  }

  free(new);

  return 0;
}

void replace(char* str, char old, char new)
{
  for(int i = 0; str[i] != '\0'; i++)
    if(str[i] == old)
      str[i] = new;
}

int check_existing(char *name, char *artist, char* song, int *exists)
{
  sqlite3 *db;
  char *msg = 0;
  int rc;
  
  rc = sqlite3_open(name, &db);
  
  char *base = "SELECT * FROM SONGS WHERE ARTIST = '%s' and NAME = '%s';";

  replace(song, '\'', '_');
  replace(song, '"', '"');
  replace(artist, '\'', '_');
  replace(artist, '"', '"');

  int l;
  char *new;
  l = snprintf(NULL, 0, base, artist, song);
  new = malloc(sizeof(char) * (l + 1));
  sprintf(new, base, artist, song);

  rc = sqlite3_exec(db, new, callback_exists, (void*)exists, &msg);

  free(new);
  sqlite3_close(db);

  return 0;
}

int insert_fingerprint(char *name, struct fingerprint *fps, int len)
{
  char *base = "INSERT INTO FINGERPRINTS (ADDRESS, OFFSET, SONG_ID)" \
               "VALUES (%d, %d, %d);";
  int l;
  char *new;
  sqlite3 *db;
  char *msg;

  sqlite3_open(name, &db);
  sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &msg);
  
  for(int i = 0; i < len; i++)
  {
    l = snprintf(NULL, 0, base, fps[i].fdt, fps[i].tID >> 32, fps[i].tID & 0xffffffff);
    new = malloc(sizeof(char) * (l + 1));

    sprintf(new, base, fps[i].fdt, fps[i].tID >> 32, fps[i].tID & 0xffffffff);

    sqlite3_exec(db, new, NULL, NULL, &msg);
    free(new);
  }

  sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &msg);
  sqlite3_close(db);

  return 0;
}

int insert_song(char *name, char *artist, char *song_name, int *id)
{
  sqlite3 *db;
  sqlite3_open(name, &db);

  char *base = "INSERT INTO SONGS (NAME, ARTIST, FINGERPRINTED)" \
               "VALUES (\"%s\", \"%s\", \"%d\");";
  int l;
  char *sql;

  replace(song_name, '\'', '_');
  replace(song_name, '"', '"');
  replace(artist, '\'', '_');
  replace(artist, '"', '"');

  l = snprintf(NULL, 0, base, song_name, artist, 1);
  sql = malloc(sizeof(char) * (l + 1));
  sprintf(sql, base, song_name, artist, 1);

  char *msg;
  int out = sqlite3_exec(db, sql, NULL, NULL, &msg);
  
  sqlite3_exec(db, "SELECT last_insert_rowid();", callback_id, id, &msg);

  free(sql);
  sqlite3_close(db);

  return out;
}

int init_bdd(char *name)
{
  int err;
  err = create_db(name);
  if(err)
    return 1;

  char *sql;
  sql = "CREATE TABLE IF NOT EXISTS SONGS(" \
          "ID             INTEGER PRIMARY KEY AUTOINCREMENT," \
          "NAME           VARCHAR(250) NOT NULL," \
          "ARTIST         VARCHAR(250) NOT NULL," \
          "FINGERPRINTED  TINYINT      DEFAULT 0" \
        ");";

  char *msg;
  err = exec_sql("fingerprints.db", sql, &msg);
  if(err)
  {
    printf("Error: %s\n", msg);
    sqlite3_free(msg);
    return 1;
  }

  sql = "CREATE TABLE IF NOT EXISTS FINGERPRINTS(" \
          "ADDRESS      INT UNSIGNED  NOT NULL," \
          "OFFSET       INT UNSIGNED  NOT NULL," \
          "SONG_ID      INT UNSIGNED  NOT NULL" \
        ");";
   
  err = exec_sql("fingerprints.db", sql, &msg);
  if(err)
  {
    printf("Error: %s\n", msg);
    sqlite3_free(msg);
    return 1;
  }
  
  sql = "CREATE INDEX IF NOT EXISTS ADDRESS ON FINGERPRINTS(ADDRESS)";
  err = exec_sql("fingerprints.db", sql, &msg);
  if(err)
  {
    printf("Error: %s\n", msg);
    sqlite3_free(msg);
    return 1;
  }

  return 0;
}
