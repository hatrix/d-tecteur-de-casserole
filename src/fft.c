/*     fft.c    */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "fft.h"

void _fft(complex double *buf, complex double *out, int n, int step)
{
  if (step < n) 
  {
    _fft(out, buf, n, step * 2);
    _fft(out + step, buf + step, n, step * 2);

    for (int i = 0; i < n; i += 2 * step)
    {
      complex double t = cexp(-I * M_PI * i / n) * out[i + step];
      buf[i / 2]     = out[i] + t;
      buf[(i + n)/2] = out[i] - t;
    }
  }
}

void fft(complex double *buf, int n)
{
  complex double *out = malloc(n * sizeof (complex double));
  for (int i = 0; i < n; i++) 
    out[i] = buf[i];

  _fft(buf, out, n, 1);
  free(out);
}


void disp(const char * s, complex double *buf, int n)
{
  printf("%s", s);
  for (int i = 0; i < n; i++)
  {
    if (!cimag(buf[i]))
      printf("%g ", creal(buf[i]));
    else
      printf("(%g, %g) ", creal(buf[i]),
          cimag(buf[i]));
  }
  printf("\n");
}
