/*    fft_m.h    */

#define M_PI 3.14159265358979323846

struct s_arg
{
  complex double *buf;
  complex double *out;
  int n;
  int step;
};

struct s_arg *make_arg(complex double *buf, complex double *out, int n, int step);

struct s_arg *deep_copy(struct s_arg *arg);

void *_fft(void *arg);

void fft(complex double *buf, int n);

void *calc_fft(void *arg);

void disp(const char *s, complex double *buf, int n);
