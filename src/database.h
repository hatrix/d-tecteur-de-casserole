#include <sqlite3.h>

int create_db(char *name);
int exec_sql(char *name, char *sql, char **msg);
int check_existing(char *name, char *artist, char* song, int *exists);
int check_address(sqlite3 *db, int *rc, uint32_t fdt, uint64_t tID, int **count, int number_IDs, int *lens);
int return_song(char *name, int ID, char **artist, char **sname);
int insert_song(char *name, char *artist, char *song_name, int *id);
int insert_fingerprint(char *name, struct fingerprint *fps, int len);
int init_bdd(char *name);
