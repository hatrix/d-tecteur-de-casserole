#include <stdint.h>
#include "frequencies.h"

struct fingerprint
{
  uint32_t fdt; // frequencies of anchor and point, delta time between
  uint64_t tID; // absolute time of anchor and song ID
};

struct fingerprint *zones(struct tf_point *tf_points, int len, int songID, int *nbfp);
