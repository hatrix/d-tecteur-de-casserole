int write_wav(char *fname, int samplerate, int numFrames, float *buf);
float *mono(long numFrames, float* buf, int *channels);
float *downsample(int *samplerate, int *numFrames, float *buf);
