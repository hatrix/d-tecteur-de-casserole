#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <string.h>
#include <stdint.h>
#include "target.h"

struct fingerprint *zones(struct tf_point *tf_points, int len, int songID, int *nb_fp)
{
  int start = 3;
  int target_size = 5;

  int size = ((len - target_size) - start) * target_size;
  struct fingerprint *fingerprints = malloc(sizeof(struct fingerprint) * size);

  int k = 0;
  for(int i = start; i < len - target_size; i++)
  {
    for(int j = 0; j < target_size; j++)
    {
      int anchor = i - start;

      // f(anchor), f(point), dt ; t(anchor), songID
      uint16_t fa = tf_points[anchor].frequency;
      uint16_t fp = tf_points[i+j].frequency;
      uint16_t dt = tf_points[i+j].time - tf_points[anchor].time;
      uint32_t part1 = (fa << (14+9)) + (fp << 14) + dt;

      uint64_t absolute_time = tf_points[anchor].time; // absolute time
      uint64_t part2 = (absolute_time << 32) + songID;

      fingerprints[k].fdt = part1; // f(a), f(p), delta time
      fingerprints[k].tID = part2; // time of anchor and songID
      k++;
    }
  }

  *nb_fp = k;
  return fingerprints;
}
