/*    fft.h    */

#define M_PI 3.14159265358979323846

void _fft(complex double buf[], complex double out[], int n, int step);

void fft(complex double buf[], int n);

void disp(const char *s, complex double buf[], int n);
