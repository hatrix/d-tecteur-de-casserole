#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <libgen.h>
#include <limits.h>

char *getExt(char *fspec)
{
  char *e = strrchr(fspec, '.');
  if(e == NULL)
    e = ""; // fast method, could also use &(fspec[strlen(fspec)]).
  return e;
}

int convert(char *file, char *tmp)
{
  char **args = malloc(sizeof(char*) * 6);
  args[0] = "ffmpeg";
  args[1] = "-y";
  args[2] = "-i";
  args[3] = file;
  args[4] = tmp;
  args[5] = NULL;

  int pid = fork();
  int status;
  if(pid)
  {
    free(args);
    waitpid(pid, &status, 0);
    if(WEXITSTATUS(status) != 0)
      return -1;
    else
      return 0;
  }
  else
  {
    int devNull = open("/dev/null", O_WRONLY);
    dup2(devNull, STDOUT_FILENO);
    dup2(devNull, STDIN_FILENO);
    dup2(devNull, STDERR_FILENO);
    close(devNull);
    execvp("ffmpeg", args);
    free(args);
    return 0;
  }
}

char *concatenate(char *d0, char *d1, int trailing)
{
  char *tmp = malloc(sizeof(char) * (strlen(d0) + strlen(d1) + 3));
  size_t l_d0 = strlen(d0);
  size_t l_d1 = strlen(d1);

  tmp = strcpy(tmp, d0);
  strcpy(tmp + l_d0, "/");
  strcpy(tmp + l_d0 + 1, d1);
  if(trailing)
    strcpy(tmp + l_d0 + l_d1 + 1, "/");
  return tmp;
}

void paths(char *origin, char ***to_convert, int *to_len, char ***ok, int *ok_len)
{
  *to_convert = malloc(sizeof(char*));
  *ok = malloc(sizeof(char*));
  *to_len = 0;
  *ok_len = 0;

  char *album;

  DIR *ar_dir; // artist dir
  struct dirent *albums; // albums dir
  ar_dir = opendir(origin);

  if (ar_dir != NULL)
  {
    while((albums = readdir(ar_dir)))
    {
      if(albums->d_name[0] != '.')
      {
        album = albums->d_name;
        struct dirent *songs;
        if(albums->d_type == 4) // directory
        {
          char *tmp = concatenate(origin, album, 1);

          DIR *al_dir = opendir(tmp);
          while((songs = readdir(al_dir)))
          {
            if(songs->d_name[0] != '.')
            {
              char *ext = getExt(songs->d_name);
              char *path = concatenate(tmp, songs->d_name, 0);

              if(strcmp(ext, ".mp3") == 0
                 || strcmp(ext, ".wav") == 0
                 || strcmp(ext, ".flac") == 0
                 || strcmp(ext, ".ogg") == 0)
              {
                if(strcmp(ext, ".mp3") == 0)
                {
                  *to_convert = realloc(*to_convert, sizeof(char*) * ((*to_len) + 1));
                  (*to_convert)[*to_len] = path;
                  (*to_len)++;
                }
                else
                {
                  *ok = realloc(*ok, sizeof(char*) * ((*ok_len) + 1));
                  (*ok)[*ok_len] = path;
                  (*ok_len)++;
                }
              }
              else
                free(path);
            }
          }
          closedir(al_dir);
          free(tmp);
        }
      }
    }
    closedir(ar_dir);
  }
  else
    perror("Couldn't open the directory");
}

