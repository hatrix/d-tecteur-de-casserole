#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>

SNDFILE* read_headers(char *path, int *frames, int *samplerate, int *channels)
{
  SNDFILE *sf; 
  SF_INFO sfinfo;
  
  sfinfo.format = 0;
  memset(&sfinfo, 0, sizeof(SF_INFO));
  sf = sf_open(path, SFM_READ, &sfinfo);
  if (sf == NULL)
  {
    printf("%s\n", sf_strerror(sf));
    exit(-1);
  }
  *frames = sfinfo.frames;
  *samplerate = sfinfo.samplerate;
  *channels = sfinfo.channels;

  return sf;
}

float* read_data(SNDFILE *sf, int nb_items, int *read)
{
  float *buf = malloc(nb_items * sizeof(float));
  *read = sf_read_float(sf, buf, nb_items);

  sf_close(sf);
  return buf;
}

void write_data(float* buf, char* path, int channels, int len)
{
  FILE *out;
  out = fopen(path, "w");
  for(int i = 0; i < len; i += channels)
  {
    for(int j = 0; j < channels; ++j)
      fprintf(out, "%f ", buf[i+j]);
    fprintf(out, "\n");
  }
  fclose(out);
}
