#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include "frequencies.h"

double magnitude(complex double c)
{
  return sqrt(pow(creal(c), 2) + pow(cimag(c), 2));
}

void hamming(float *buf, int nb_windows, int n)
{
  for(int i = 0; i < nb_windows; i++) // loop over windows
  {
    for(int j = 0; j < n; j++) // loop over window
    {
      // hamming function
      float h = .54 - .46 * cos(2*3.1416*j/(n-1));
      buf[n*i+j] *= h;
    }
  }
}

int max(complex double *buf_c, int left, int right, double *mag)
{
  double max = buf_c[left];
  int index = 0;
  for(int j = left; j < right; j++) // FFT is symmetrical
  {
    *mag = magnitude(buf_c[j]);
    if(*mag > max)
    {
      max = *mag;
      index = j;
    }
  }

  *mag = max;
  return index;
}

// expecting n to be 1024
int *frequency(complex double *buf_c, int *len)
{
  double *mags = malloc(6 * sizeof(double));
  int *freqs = malloc(6 * sizeof(int));
  
  freqs[0] = max(buf_c, 0, 10, &mags[0]);
  freqs[1] = max(buf_c, 10, 20, &mags[1]);
  freqs[2] = max(buf_c, 20, 40, &mags[2]);
  freqs[3] = max(buf_c, 40, 80, &mags[3]);
  freqs[4] = max(buf_c, 80, 160, &mags[4]);
  freqs[5] = max(buf_c, 160, 511, &mags[5]);

  double average = 0;
  for(int i = 0; i < 6; i++)
    average += mags[i];
  average /= 6;

  int *above = malloc(6 * sizeof(double)); // frequencies to keep
  *len = 0; // length of above
  int coeff = 1;
  for(int i = 0; i < 6; i++)
  {
    if(mags[i] > average * coeff)
    {
      above[*len] = freqs[i];
      (*len)++;
    }
  }

  free(freqs);
  free(mags);

  return above;
}

float *lowpass(float* buf, int cutoff, int samplerate, int length)
{
  float RC = 1.0/(cutoff*2*3.14);
  float dt = 1.0/samplerate;
  float alpha = dt/(RC+dt);
  float *new_buf = malloc(length * sizeof(float));

  new_buf[0] = buf[0];
  for(int i = 1; i < length; i++)
    new_buf[i] = new_buf[i-1] + (alpha * (buf[i] - new_buf[i-1]));

  free(buf);

  return new_buf;
}
