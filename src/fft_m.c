/*     fft_m.c    */

#define _POSIX_C_SOURCE 199303L
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <pthread.h>
#include "fft_m.h"

void *_fft(void *arg)
{
  struct s_arg *args = arg;

  if (args->step < args->n) 
  {
    struct s_arg *arg_c = make_arg(args->out + args->step, 
        args->buf + args->step, args->n, args->step * 2);
    pthread_t thread;
    if (pthread_create(&thread, NULL, _fft, arg_c) != 0)
      printf("Create failed\n");
    //_fft(args->out + args->step, args->buf + args->step, args->n, args->step * 2);

    _fft(arg);
    pthread_join(thread, NULL);
    free(arg_c);
      //    args->step /= 2;
      // call calc}
    args->step /= 2;
  }

    calc_fft(args);
    return NULL;
}

void *calc_fft(void *arg)
{
  
  struct s_arg *args = arg;
  if (args->step > args->n)
    pthread_exit(NULL);
  for (int i = 0; i < args->n; i += 2 * args->step)
  {
    complex double t = cexp(-I * M_PI * i / args->n) * args->out[i + args->step];
    args->buf[i / 2]     = args->out[i] + t;
    args->buf[(i + args->n)/2] = args->out[i] - t;
  }
  pthread_exit(NULL);
}

struct s_arg *make_arg(complex double *buf, complex double *out, int n, int step)
{
  struct s_arg *arg = malloc(sizeof (struct s_arg));
  arg->out = out;
  arg->buf = buf;
  arg->n = n;
  arg->step = step;
  return arg;
}


struct s_arg *deep_copy(struct s_arg *arg)
{
  struct s_arg *args_c = malloc(sizeof (struct s_arg));
  args_c->out = arg->out;
  args_c->buf = arg->buf;
  args_c->n = arg->n;
  args_c->step = arg->step;
  return args_c;
}



void fft(complex double *buf, int n)
{
  complex double *out = malloc(n * sizeof (complex double));
  for (int i = 0; i < n; i++) 
    out[i] = buf[i];
  struct s_arg *args = malloc(sizeof (struct s_arg));
  args->out = out;
  args->buf = buf;
  args->n = n;
  args->step = 1;
  _fft((void *)args);

}


void disp(const char * s, complex double *buf, int n)
{
  printf("%s", s);
  for (int i = 0; i < n; i++)
  {
    if (!cimag(buf[i]))
      printf("%g ", creal(buf[i]));
    else
      printf("(%g, %g) ", creal(buf[i]),
          cimag(buf[i]));
  }
  printf("\n");
}
