#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include <libgen.h>
#include "../fft.h"
#include "../read_file.h"
#include "../write_wav.h"
#include "../tools.h"
#include "../target.h"
#include "../database.h"
#include "../convert.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  char **to_convert;
  char **ok;
  int to_len;
  int ok_len;
  
  paths(argv[1], &to_convert, &to_len, &ok, &ok_len);

  char **songs = malloc(sizeof(char*) * (to_len + ok_len));
  for(int i = 0; i < to_len; i++)
    songs[i] = to_convert[i];
  for(int i = 0; i < ok_len; i++)
    songs[to_len + i] = ok[i];

  printf("%d already ok, %d to convert\n", ok_len, to_len);
  for(int i = 0; i < to_len + ok_len; i++)
    printf("[%d] %s\n", i, basename(songs[i]));

  char *artist = basename(argv[1]);
  
  SNDFILE *sf;

  printf("\nRunning through %s's songs\n", artist);
  printf("%d songs to treat\n\n", ok_len + to_len);

  char *bdd = "fingerprints.db";
  init_bdd(bdd);

  char *tmp_path = "/tmp/music.wav";
  char *path;
  for(int i = 0; i < to_len + ok_len; i++)
  {
    printf("\x1B[32m[%d] %s\n\x1B[0m", i+1, basename(songs[i]));

    // Check is song already in database
    int exists = 0;
    check_existing(bdd, artist, basename(songs[i]), &exists);
    
    if(exists)
    {
      printf(" - Song already exists in database, continuing…\n\n");
      continue;
    }

    path = songs[i];
    if(strcmp(getExt(songs[i]), ".mp3") == 0)
    {
      printf(" - Converting to .wav\n");
      convert(songs[i], tmp_path);
      path = tmp_path;
    }

    // Read headers
    int frames, samplerate, channels;
    int nb_items;
    sf = read_headers(path, &frames, &samplerate, &channels);

    // Read data
    float *buf;
    int len;
    nb_items = frames * channels;
    buf = read_data(sf, nb_items, &len);

    // convert to mono and re-read
    if(channels > 1)
      buf = mono(frames, buf, &channels);

    // Lowpass filter
    puts(" - Applying low-pass filter");
    buf = lowpass(buf, 5000, samplerate, frames);

    // Downsampling to 10025Hz
    puts(" - Downsampling to 11 025Hz");
    buf = downsample(&samplerate, &frames, buf);

    // number of windows
    int n = samplerate / 10.7666015625; // We want a 10.7Hz precision
    int nb_windows = frames / n;
    //printf("%d items, %d windows\n", frames, nb_windows);

    // window function
    puts(" - Applying a window function");
    hamming(buf, nb_windows, n);

    // Convert to complex doubles
    complex double *buf_c = float_to_c_double(buf, frames);

    int size = nb_windows * 6; // maximum number of points
    int used = 0;
    struct tf_point *tf_points = malloc(sizeof(struct tf_point) * size);

    puts(" - Searching for frequencies via FFT");
    for(int i = 0; i < nb_windows; i++)
    {
      // execute FFT
      fft(buf_c + n*i, n);
      
      // find frequency
      int l = 0;
      int *f = frequency(buf_c + n*i, &l);
      
      for(int j = 0; j < l; j++)
      {
        tf_points[used].frequency = f[j]; // index on 512 array
        tf_points[used].time = i; // absolute index (current window);
        used++;
      }
      free(f);
    }

    int nb_fp;
    int sID;

    printf(" - Inserting fingerprints into DB\n");

    insert_song(bdd, artist, basename(songs[i]), &sID);
    struct fingerprint* fingerprints = zones(tf_points, used, sID, &nb_fp);
    insert_fingerprint(bdd, fingerprints, nb_fp);

    printf(" - Song has ID %d\n", sID);
  
    puts("");

    free(fingerprints);
    free(tf_points);
    free(buf_c);
    free(buf);
  }
  printf("\nDone!\n");
  
  for(int i = 0; i < to_len; i++)
    free(to_convert[i]);
  for(int i = 0; i < ok_len; i++)
    free(ok[i]);
  free(ok);
  free(to_convert);
  free(songs);

  return 0;
}
