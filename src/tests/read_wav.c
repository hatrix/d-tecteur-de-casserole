#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <string.h>
#include "../read_file.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(argv[1], &frames, &samplerate, &channels);

  printf("frames     = %d\n", frames);
  printf("samplerate = %d\n", samplerate);
  printf("channels   = %d\n", channels);
  nb_items = frames * channels;
  printf("num_items  = %d\n", nb_items);
  printf("seconds    = %f\n", (float)frames/samplerate);

  // Read data
  float *buf;
  int len;
  buf = read_data(sf, nb_items, &len);
  //printf("Read %d items\n", len);

  // Write data to file
  write_data(buf, "data.out", channels, len); 
  return 0;
}
