#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include "../read_file.h"
#include "../write_wav.h"

// Convert wav file to mono
int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(argv[1], &frames, &samplerate, &channels);

  // Read data
  float *buf;
  int len;
  nb_items = frames * channels;
  buf = read_data(sf, nb_items, &len);

  printf("size buf: %d\n", len);
  buf = mono(frames, buf, &channels);

  write_wav("test.wav", samplerate, frames, buf);

  return 0;
}
