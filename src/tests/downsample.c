#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include "../read_file.h"
#include "../frequencies.h"
#include "../write_wav.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(argv[1], &frames, &samplerate, &channels);

  // Read data
  float *buf;
  int len;
  nb_items = frames * channels;
  buf = read_data(sf, nb_items, &len);

  // convert to mono
  if(channels > 1)
    buf = mono(frames, buf, &channels);

  buf = lowpass(buf, 5000, samplerate, frames);
  buf = downsample(&samplerate, &frames, buf);
  write_wav("test.wav", samplerate, frames, buf);

  return 0;
}
