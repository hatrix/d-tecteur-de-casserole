#include <stdio.h>
#include <math.h>
#include <complex.h>
#if 0
#include "../fft.h"
#else
#include "../fft_m.h"
#endif
int main()
{
  complex double buf[] = {1, 1, 1, 1, 0, 0, 0, 0};

  disp("Data: ", buf, 8);
  fft(buf, 8);
  disp("\nFFT : ", buf, 8);

  return 0;
}
