#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include <libgen.h>
#include <sqlite3.h>
#include <unistd.h>
#include "../fft.h"
#include "../read_file.h"
#include "../write_wav.h"
#include "../tools.h"
#include "../target.h"
#include "../database.h"
#include "../convert.h"
#include "../hashmap.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  printf("\x1B[32mStarting Recognition\x1B[0m");
  printf("                                                       |\n");

  char *bdd = "fingerprints.db";
  
  char *path = argv[1];
  char *tmp_path = "/tmp/music.wav";
  if(strcmp(getExt(path), ".mp3") == 0)
  {
    printf(" - Converting to .wav\n");
    convert(path, tmp_path);
    path = tmp_path;
  }

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(path, &frames, &samplerate, &channels);

  // Read data
  float *buf;
  int len;
  nb_items = frames * channels;
  buf = read_data(sf, nb_items, &len);

  // convert to mono and re-read
  if(channels > 1)
    buf = mono(frames, buf, &channels);

  // Lowpass filter
  puts(" - Applying low-pass filter");
  buf = lowpass(buf, 5000, samplerate, frames);

  // Downsampling to 10025Hz
  puts(" - Downsampling to 11 025Hz");
  buf = downsample(&samplerate, &frames, buf);

  // number of windows
  int n = samplerate / 10.7666015625; // We want a 10.7Hz precision
  int nb_windows = frames / n;
  //printf("%d items, %d windows\n", frames, nb_windows);

  // window function
  puts(" - Applying a window function");
  hamming(buf, nb_windows, n);

  // Convert to complex doubles
  complex double *buf_c = float_to_c_double(buf, frames);

  int size = nb_windows * 6; // maximum number of points
  int used = 0;
  struct tf_point *tf_points = malloc(sizeof(struct tf_point) * size);

  puts(" - Searching for frequencies via FFT");
  for(int i = 0; i < nb_windows; i++)
  {
    // execute FFT
    fft(buf_c + n*i, n);
    
    // find frequency
    int l = 0;
    int *f = frequency(buf_c + n*i, &l);
    
    for(int j = 0; j < l; j++)
    {
      tf_points[used].frequency = f[j]; // index on 512 array
      tf_points[used].time = i; // absolute index (current window);
      used++;
    }
    free(f);
  }

  int nb_fp;
  struct fingerprint* fingerprints = zones(tf_points, used, 0, &nb_fp);

  printf(" - Recognising\n");
  //uint64_t sID;

  sqlite3 *db;
  char *msg;
  int rc;

  rc = sqlite3_open(bdd, &db);
  sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &msg);

  // Get number of IDs
  sqlite3_stmt *stmt;
  char *count_ID = "SELECT COUNT(ID) FROM SONGS;";
  sqlite3_prepare_v2(db, count_ID, strlen(count_ID), &stmt, NULL);
  sqlite3_step(stmt);
  const unsigned char* result_count = sqlite3_column_text(stmt, 0);

  int number_IDs = atoi((const char*)result_count) + 1;
  //printf("number: %d\n", number_IDs);

  // Get time differences
  int **count = calloc(number_IDs, sizeof(int*));
  for(int i = 0; i < number_IDs; i++)
    count[i] = calloc(1, sizeof(int*));

  int *lens = calloc(number_IDs, sizeof(int));

  float prog = -1; // progress bar
  float psize = 0; // progress on screen
  int count_s = 0;
  int wsize = 70;
  printf("  -[>");
  fflush(stdout);
  for(int i = 0; i < nb_fp; i++)
  {
    check_address(db, &rc, fingerprints[i].fdt, fingerprints[i].tID, count, number_IDs, lens);

    psize = i / (float)nb_fp * wsize;
    if((int)psize != (int)prog)
    {
      if(nb_fp > wsize)
      {
        if(count_s > prog/(float)wsize)
        {
          printf("\b=>");
          prog = psize;
          fflush(stdout);
        }
      }
      else
      {
        printf("\b%.*s>", wsize/nb_fp, "======================");
        prog = psize;
        fflush(stdout);
      }
      count_s++;
    }
  }
  printf("]\n");

  sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &msg);
  sqlite3_close(db);


  // ------------------------
  // Hack, because python ftw 
  // ------------------------

  // Store the datas in tmp
  FILE *f = fopen("/tmp/music_out", "w");
  fprintf(f, "nb_fp: %d\n", nb_fp);
  for(int i = 0; i < number_IDs; i++)
  {
    fprintf(f, "ID: %d\n", i);
    for(int j = 0; j < lens[i]; j++)
      fprintf(f, "%u\n", count[i][j]);
  }
  fclose(f);

  // free a bunch of stuff
  free(fingerprints);
  free(tf_points);
  free(buf_c);
  free(buf);

  // recognize the music
  printf(" - Best matching songs:\n");
  char **args = malloc(sizeof(char*) * 4);
  args[0] = "python3";
  args[1] = "recog.py";
  args[2] = "/tmp/music_out";
  args[3] = NULL;
  execvp("python3", args);
  
  return 0;
}
