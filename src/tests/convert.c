#include <libgen.h>
#include <stdlib.h>
#include <stdio.h>
#include "../convert.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  char **to_convert;
  char **ok;
  int to_len;
  int ok_len;
  
  paths(argv[1], &to_convert, &to_len, &ok, &ok_len);
  char *artist = basename(argv[1]);

  printf("Artist: %s\n\n", artist);
  printf("to convert: %d\n", to_len);
  printf("already ok: %d\n", ok_len);

  puts("\nTo convert:");
  for(int i = 0; i < to_len; i++)
  {
    //convert(to_convert[i], "/tmp/music.wav");
    printf("%s\n", to_convert[i]);
  }
  
  puts("\nAlready ok:");
  for(int i = 0; i < ok_len; i++)
    printf("%s\n", ok[i]);

  for(int i = 0; i < to_len; i++)
    free(to_convert[i]);
  for(int i = 0; i < ok_len; i++)
    free(ok[i]);
  free(to_convert);
  free(ok);

  return 0;
}
