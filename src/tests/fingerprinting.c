#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include "../fft.h"
#include "../read_file.h"
#include "../write_wav.h"
#include "../tools.h"
#include "../target.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(argv[1], &frames, &samplerate, &channels);

  // Read data
  float *buf;
  int len;
  nb_items = frames * channels;
  buf = read_data(sf, nb_items, &len);

  // convert to mono and re-read
  if(channels > 1)
    buf = mono(frames, buf, &channels);

  // Lowpass filter
  buf = lowpass(buf, 5000, samplerate, frames);

  // Downsampling to 10025Hz
  buf = downsample(&samplerate, &frames, buf);

  // number of windows
  int n = samplerate / 10.7666015625; // We want a 10.7Hz precision
  int nb_windows = frames / n;
  printf("%d items, %d windows\n", frames, nb_windows);

  // window function
  hamming(buf, nb_windows, n);

  // Convert to complex doubles
  complex double *buf_c = float_to_c_double(buf, frames);

  int size = nb_windows * 6; // maximum number of points
  int used = 0;
  struct tf_point *tf_points = malloc(sizeof(struct tf_point) * size);

  for(int i = 0; i < nb_windows; i++)
  {
    // execute FFT
    fft(buf_c + n*i, n);
    
    // find frequency
    int l = 0;
    int *f = frequency(buf_c + n*i, &l);
    
    for(int j = 0; j < l; j++)
    {
      tf_points[used].frequency = f[j]; // index on 512 array
      tf_points[used].time = i; // absolute index (current window);
      used++;
    }
    free(f);
  }

  //for(int i = 0; i < used; i++)
  //  printf("elt: %d, t: %d, f: %d\n", i, tf_points[i].time, tf_points[i].frequency);
  
  int nb_fp;
  // songID for test
  struct fingerprint *fingerprints = zones(tf_points, used, 80, &nb_fp);

  free(fingerprints);
  free(tf_points);
  free(buf_c);
  free(buf);
  
  return 0;
  
  printf("%d fingerprints generated\n", nb_fp);

  for(int i = 0; i < nb_fp; i++)
    printf("Element %d: fdt: %d, tID: %lu\n", i, fingerprints[i].fdt,
                                             fingerprints[i].tID);

  return 0;
}
