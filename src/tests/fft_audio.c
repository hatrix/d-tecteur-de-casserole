#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>
#include "../fft.h"
#include "../read_file.h"
#include "../frequencies.h"
#include "../write_wav.h"
#include "../tools.h"

int main(int argc, char **argv)
{
  if(argc < 2)
    return -1;

  SNDFILE *sf;

  // Read headers
  int frames, samplerate, channels;
  int nb_items;
  sf = read_headers(argv[1], &frames, &samplerate, &channels);

  // Read data
  float *buf;
  int len;
  nb_items = frames * channels;
  buf = read_data(sf, nb_items, &len);

  // convert to mono and re-read
  if(channels > 1)
    buf = mono(frames, buf, &channels);

  // Lowpass filter
  buf = lowpass(buf, 5000, samplerate, frames);

  // Downsampling to 10025Hz
  buf = downsample(&samplerate, &frames, buf);

  // number of windows
  int n = samplerate / 10.7666015625; // We want a 10.7Hz precision
  int nb_windows = frames / n;
  printf("%d items, %d windows\n", frames, nb_windows);

  // window function
  hamming(buf, nb_windows, n);

  // Convert to complex doubles
  complex double *buf_c = float_to_c_double(buf, frames);

  for(int i = 0; i < nb_windows; i++)
  {
    // execute FFT
    fft(buf_c + n*i, n);
    
    // find frequency
    int l = 0;
    int *f = frequency(buf_c + n*i, &l);
    
    for(int j = 0; j < l; j++)
      printf("Time: %d, Freq: %d Hz\n", i, f[j]*samplerate/n);
    puts("");

    free(f);
  }

  free(buf_c);
  free(buf);

  return 0;
}
