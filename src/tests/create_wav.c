// Andrew Greensted - Feb 2010
// http://www.labbookpages.co.uk
// Version 1

#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <sndfile.h>

int create(char *fname, double *buffer, int sampleRate, int length)
{
  // Set file settings, 16bit Mono PCM
  SF_INFO info;
  info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  info.channels = 1;
  info.samplerate = sampleRate;

  // Open sound file for writing
  SNDFILE *sndFile = sf_open(fname, SFM_WRITE, &info);

  // Write frames
  sf_writef_double(sndFile, buffer, length);

  // Tidy up
  sf_write_sync(sndFile);
  sf_close(sndFile);
  free(buffer);

  return 0;
}

int main()
{
  int sampleRate = 44100; // frames / second

  // number of frequencies
  int n = 23;
  
  // array of frequencies
  double n0 = 0;
  double n1 = 261.3;
  double n2 = 311.1;
  double n3 = 349.2;
  double n4 = 369.9;
  double *freq = malloc(n * sizeof(double));
  freq[0] = n1;
  freq[1] = n0;
  freq[2] = n2;
  freq[3] = n0;
  freq[4] = n3;
  freq[5] = n0;
  freq[6] = n1;

  freq[7] = n0;
  freq[8] = n2;
  freq[9] = n0;
  freq[10] = n4;
  freq[11] = n3;
  freq[12] = n0;

  freq[13] = n1;
  freq[14] = n0;
  freq[15] = n2;
  freq[16] = n0;
  freq[17] = n3;
  freq[18] = n0;
  freq[19] = n2;

  freq[20] = n0;
  freq[21] = n1;
  freq[22] = n0;

  // duration of each freq in seconds
  double *duration = malloc(n * sizeof(double)); 
  duration[0] = 0.5;
  duration[1] = 0.5;
  duration[2] = 0.5;
  duration[3] = 0.5;
  duration[4] = 1;
  duration[5] = 0.5;
  duration[6] = 0.5;

  duration[7] = 0.5;
  duration[8] = 0.5;
  duration[9] = 0.5;
  duration[10] = 0.5;
  duration[11] = 1;
  duration[12] = 1;
  
  duration[13] = 0.5;
  duration[14] = 0.5;
  duration[15] = 0.5;
  duration[16] = 0.5;
  duration[17] = 1;
  duration[18] = 0.5;
  duration[19] = 0.5;
  
  duration[20] = 0.5;
  duration[21] = 2.5;
  duration[22] = 1;

  for(int i = 0; i < n; i++)
    duration[i] /= 1.70;

  // length
  int length = 0;
  for(int i = 0; i < n; i++)
    length += duration[i] * sampleRate;

  // Allocate storage for frames
  double *buffer = (double *) malloc(length * sizeof(double));

  // Create song
  int written = 0;
  for(int i = 0; i < n; i++)
  {
    int numFrames = duration[i] * sampleRate;
    for (int f = 0 ; f < numFrames ; f++)
    {
      double time = f * duration[i] / numFrames;
      buffer[written] = sin(2.0 * 3.1415 * time * freq[i]);
      written++;
    }
  }

  create("test.wav", buffer, sampleRate, length);

  return 0;
}
