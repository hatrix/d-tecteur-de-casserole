// Andrew Greensted - Feb 2010
// http://www.labbookpages.co.uk
// Version 1

#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <sndfile.h>
#include "write_wav.h"

float *mono(long numFrames, float* buf, int *channels)
{
  // Allocate storage for frames
  float *buffer = calloc(numFrames, sizeof(float));
  for(int h = 0; h < *channels; h++){
    int j = 0;
    for(int i = h; i < numFrames * *channels; i += *channels)
    {
      buffer[j] += buf[i] / *channels;
      j++;
    }
  }

  free(buf);
  *channels = 1;
  return buffer;
}

// We here assume it's a one channel sound
float *downsample(int *samplerate, int *numFrames, float *buf)
{
  // target : 11025 Hz
  int factor = *samplerate / 11025;

  // Allocate storage for frames
  float *buffer = calloc(*numFrames / factor + 1, sizeof(float));
  for(int i = 0; i < *numFrames; i++)
    buffer[i/factor] += buf[i];

  for(int i = 0; i < *numFrames / factor; i++)
    buffer[i] /= factor;

  *samplerate /= factor;
  *numFrames /= factor;

  free(buf);

  return buffer;
}

int write_wav(char *fname, int samplerate, int numFrames, float *buf)
{
  // Set file settings, 16bit Mono PCM
  SF_INFO info;
  info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  info.channels = 1;
  info.samplerate = samplerate;

  // Open sound file for writing
  SNDFILE *sndFile = sf_open(fname, SFM_WRITE, &info);

  // Write frames
  long writtenFrames = sf_writef_float(sndFile, buf, numFrames);

  // Check correct number of frames saved
  if (writtenFrames != numFrames) {
    fprintf(stderr, "Did not write enough frames for source\n");
    sf_close(sndFile);
    free(buf);
    return -1;
  }

  // Tidy up
  sf_write_sync(sndFile);
  sf_close(sndFile);
  free(buf);

  return 0;
}
