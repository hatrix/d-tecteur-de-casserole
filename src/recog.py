#!/usr/bin/env python3

from sys import argv
import sqlite3

def green(s):
    return "\x1B[32m{}\x1B[0m".format(s)

f = open(argv[1])
f = f.readlines()
nb_fp = int(f[0].split()[1])
f = f[1:]

# Count the occurence of each difference per ID
count = {}
for line in f:
    if line.startswith("ID:"):
        ID = int(line[4:])
        count[ID] = {}
    else:
        if int(line) in count[ID].keys():
            count[ID][int(line)] += 1
        else:
            count[ID][int(line)] = 1

# Get the maximum of occurences
maxs = {}
NB = len(count)
for i in range(NB):
    if i not in maxs.keys():
        maxs[i] = 0
    try:
        for val in count[i].keys():
            if count[i][val] > count[i][maxs[i]]:
                maxs[i] = val
    except:
        pass

# Number of maximum same occurences per ID
m2 = {}
for i in range(NB):
    try:
        m2[i] = count[i][maxs[i]]
    except:
        m2[i] = 0

# Sort everything
m = sorted(m2, key=m2.__getitem__, reverse=True)

# Query the first three matches
conn = sqlite3.connect('fingerprints.db')
c = conn.cursor()

first = list(list(c.execute("SELECT * FROM SONGS WHERE ID=?", (m[0],)))[0])
first[1] = green('.'.join(first[1].split('.')[:-1]))

second = list(list(c.execute("SELECT * FROM SONGS WHERE ID=?", (m[1],)))[0])
second[1] = green('.'.join(second[1].split('.')[:-1]))

third = list(list(c.execute("SELECT * FROM SONGS WHERE ID=?", (m[2],)))[0])
third[1] = green('.'.join(third[1].split('.')[:-1]))


p1 = m2[m[0]]/nb_fp*100
p2 = m2[m[1]]/nb_fp*100
p3 = m2[m[2]]/nb_fp*100
print("   - {:45s} by {}".format(first[1], green(first[2])))
print("   - {:45s} by {}".format(second[1], green(second[2])))
print("   - {:45s} by {}".format(third[1], green(third[2])))
