#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <sndfile.h>
#include <string.h>

complex double *float_to_c_double(float *buf, int length)
{
  complex double *buf_c;
  buf_c = malloc(sizeof(complex double) * length); // complex buffer
  for(int i = 0; i < length; i++)
    buf_c[i] = (double)buf[i] + 0.0f *_Complex_I;

  return buf_c;
}
