SNDFILE* read_headers(char *path, int *frames, int *samplerate, int *channels);
float* read_data(SNDFILE *sf, int nb_items, int *read);
void write_data(float* buf, char* path, int channels, int len);
